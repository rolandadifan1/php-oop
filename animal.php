<?php 

class animal{

	public $name;
	public $leg;
	public $cold_blooded;

	public function __construct($name, $leg, $cold_blooded){
		$this->name = $name;
		$this->leg = $leg;
		$this->cold_blooded =$cold_blooded;
		

	}

	public function set_name($name){
		$this->name = $name;
	}
	public function get_name(){
		return $this->name;
	}
	public function set_leg($leg){
		$this->leg = $leg;
	}
	public function get_leg(){
		return $this->leg;
	}
	public function set_cold_blooded($cold_blooded){
		$this->cold_blooded = $cold_blooded;
	}
	public function get_cold_blooded(){
		return $this->cold_blooded;
	}
}

 ?>